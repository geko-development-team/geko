# Geko Server Application

The Geko Project encompasses a group of ambitious projects and ideas, aiming to create a digital environment tailored to the needs of Gibson Ek High School.

The primary goals of the Geko Project are as follows:

- Create a one-stop hub for Gibson Ek students and staff, empowering them to:
  - Manage projects and track academic progress & goals
  - Keep inventory on Maker Space equipment and materials
  - Host and keep up with Gibson Ek offerings and events
  - ..and much more!

- Provide Gibson Ek with a self-hosted service that eliminates reliance on external services such as Google Cloud and LiFT

- Build a set of scalable, maintainable applications that allow for easy upkeep by future Gibson Ek developers.

# For Geko developers & contributors
As maintainability is Geko's top priority, all of Geko's codebase is to be **thoroughly documented and readable** by all current and future Geko developers and contributors.
