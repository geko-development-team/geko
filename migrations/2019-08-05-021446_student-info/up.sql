-- Your SQL goes here
CREATE TABLE student_accounts (
  email TEXT PRIMARY KEY,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL
);

CREATE TABLE student_academics (
  account_email TEXT PRIMARY KEY REFERENCES student_accounts(email),
  academic_standing TEXT NOT NULL DEFAULT '101',
  competency_count SMALLINT NOT NULL DEFAULT 0,
  aleks_percentage SMALLINT NOT NULL DEFAULT 0,
  FOREIGN KEY (account_email) REFERENCES student_accounts(email) 
) 
