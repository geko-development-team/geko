use crate::schema::*;
use serde::*;

#[derive(Serialize, Queryable, Identifiable, Associations)]
#[table_name="student_accounts"]
#[primary_key("account_id")]
pub struct StudentAccount {
    pub email: String,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Queryable, Identifiable, Associations)]
#[table_name="student_academics"]
#[primary_key("account_id")]
#[belongs_to(StudentAccount, foreign_key="account_email")]
pub struct StudentAcademics {
    pub account_email: String,
    pub academic_standing: String,
    pub competency_count: i16,
    pub aleks_percentage: i16
}

#[derive(Insertable)]
#[table_name="student_accounts"]
pub struct NewAccount {
    pub email: String,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Insertable)]
#[table_name="student_academics"]
pub struct NewAcademicProfile {
    pub account_email: String
}
