#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
use rocket::http::*;
use rocket_contrib::serve::*;
use rocket_cors::{AllowedHeaders, AllowedOrigins, Error};
use dotenv::dotenv;
mod routes;
use crate::routes::*;

fn main() -> Result<(), Error> {
    dotenv()
        .expect("Failed to read .env file, is it missing?");
    
    // let database_connection = establish_connection();

    // Initialize Cross-Origin Resource Sharing settings
    let allowed_origins = AllowedOrigins::some_exact(&["http://localhost:8000"]);
    let cors = rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&["Authorization", "Accept"]),
        allow_credentials: true,
        ..Default::default()
    }.to_cors()?;

    // Launch the server
    rocket::ignite()
        .mount("/", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/web")))
        .mount("/", routes![authenticate_token])
    	.attach(cors)
    	.launch();

    Ok(())
}
