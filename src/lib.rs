pub mod schema;
pub mod models;
pub mod auth;

#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use std::env;

// Returns a connection to the database currently in use; the URL is pulled from a '.env' file.
pub fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    let connection = PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url));
    println!("Database connection successful...");

    connection
}
