#![feature(proc_macro_hygiene, decl_macro)]
extern crate rocket;
use rocket::response::status;
use rocket::http::{RawStr, Status};

#[post("/token", format = "application/x-www-form-urlencoded", data = "<auth_token>")]
pub fn authenticate_token(auth_token: String) -> status::Custom<String> {
    status::Custom(Status::Ok, auth_token)
}
