table! {
    student_academics (account_email) {
        account_email -> Text,
        academic_standing -> Text,
        competency_count -> Int2,
        aleks_percentage -> Int2,
    }
}

table! {
    student_accounts (email) {
        email -> Text,
        first_name -> Text,
        last_name -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    student_academics,
    student_accounts,
);
